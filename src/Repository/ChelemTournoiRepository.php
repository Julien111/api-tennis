<?php

namespace App\Repository;

use App\Entity\ChelemTournoi;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ChelemTournoi|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChelemTournoi|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChelemTournoi[]    findAll()
 * @method ChelemTournoi[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChelemTournoiRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChelemTournoi::class);
    }

    // /**
    //  * @return ChelemTournoi[] Returns an array of ChelemTournoi objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ChelemTournoi
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
