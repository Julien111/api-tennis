<?php

namespace App\Repository;

use App\Entity\JoueursTennis;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method JoueursTennis|null find($id, $lockMode = null, $lockVersion = null)
 * @method JoueursTennis|null findOneBy(array $criteria, array $orderBy = null)
 * @method JoueursTennis[]    findAll()
 * @method JoueursTennis[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JoueursTennisRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JoueursTennis::class);
    }

    // /**
    //  * @return JoueursTennis[] Returns an array of JoueursTennis objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JoueursTennis
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
