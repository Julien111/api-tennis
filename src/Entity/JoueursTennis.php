<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\JoueursTennisRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(collectionOperations={"get"={"normalization_context"={"groups"="collection:read"}}})
 * @ORM\Entity(repositoryClass=JoueursTennisRepository::class) 
 */
class JoueursTennis
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"collection:read"}) 
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"collection:read"}) 
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"collection:read"}) 
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $sexe;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\OneToOne(targetEntity=ChelemTournoi::class, mappedBy="joueurVainqueur", cascade={"persist", "remove"})
     */
    private $chelemTournoi;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getChelemTournoi(): ?ChelemTournoi
    {
        return $this->chelemTournoi;
    }

    public function setChelemTournoi(ChelemTournoi $chelemTournoi): self
    {
        // set the owning side of the relation if necessary
        if ($chelemTournoi->getJoueurVainqueur() !== $this) {
            $chelemTournoi->setJoueurVainqueur($this);
        }

        $this->chelemTournoi = $chelemTournoi;

        return $this;
    }
}