<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ChelemTournoiRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ChelemTournoiRepository::class)
 */
class ChelemTournoi
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $vainqueur;

    /**
     * @ORM\OneToOne(targetEntity=JoueursTennis::class, inversedBy="chelemTournoi", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $joueurVainqueur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVainqueur(): ?bool
    {
        return $this->vainqueur;
    }

    public function setVainqueur(bool $vainqueur): self
    {
        $this->vainqueur = $vainqueur;

        return $this;
    }

    public function getJoueurVainqueur(): ?JoueursTennis
    {
        return $this->joueurVainqueur;
    }

    public function setJoueurVainqueur(JoueursTennis $joueurVainqueur): self
    {
        $this->joueurVainqueur = $joueurVainqueur;

        return $this;
    }
}
